;    Waylos, a kernel built in rust
;    Copyright (C) 2015 Waylon Cude
;
;    This program is free software: you can redistribute it and/or modify
;    it under the terms of the GNU General Public License as published by
;    the Free Software Foundation, either version 3 of the License, or
;    (at your option) any later version.
;
;    This program is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU General Public License for more details.
;
;    You should have received a copy of the GNU General Public License
;    along with this program.  If not, see <http://www.gnu.org/licenses/>.
%include 'src/arch/x86-64/save.mac'

global system_call
global print_system
extern syscall_stack_end
extern print_1

print_system:
  mov r10,1
  syscall
  ret

system_call:
  SAVEALL ;Unneccessary, all registers don't need to be saved
  mov rbp,rsp
  mov rsp, syscall_stack_end
  push syscall_finish
  jmp [syscall_list+8*r10]
syscall_list:
  dq syscall_finish
  dq print_1



  syscall_finish:
  mov rsp,rbp
  RESTOREALL
  sysret
  


; vim: ft=nasm
