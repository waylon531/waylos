# Waylos
Waylos is going to be a 64-bit microkernel written in Rust.

To build waylos you need the nightly version of rust. Waylos uses Hydrogen as a loader.

To build from source:
```
git clone https://gitlab.com/waylon531/waylos.git
git submodule update --init
make run
```

To run with runtime tests:
```
make test
make run
```


Current features:
* A watermark memory allocator
* x86-64 only
* Printing to the screen
* Paging
* Pages are allocated on page fault

Planned features:
* Multithreading (WIP)
* Message passing

Design Philosophy:
* 1 physical computer != 1 virtual computer
* Everything that can be run in usermode should be
* Drivers should be able to crash without taking down the whole computer
